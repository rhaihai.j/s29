const express = require('express')
const app=express();
app.use(express.urlencoded({extended:true}));
app.use(express.json());
const port = 3000;

let users = [];

app.get('/home',(req,res)=>{
	res.send('This is home')
})

app.get('/users',(req,res)=>{
	res.send(users)
})

app.post('/add-user',(req,res)=>{
	users.push(req.body);
	res.send(`Added user!`)
})

app.listen(port,()=>{
	console.log(`The server is running at port ${port}`);
})

